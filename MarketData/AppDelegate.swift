//
//  AppDelegate.swift
//  MarketData
//
//  Created by Ghenadie Munteanu on 7/13/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    let routControllerdentifier = "StocksViewController"
    let config = Config()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()

        let rootController = StocksViewController(nibName: routControllerdentifier, bundle: nil)
        rootController.endpoints = config.endPoints

        let nav = UINavigationController(rootViewController: rootController)
        nav.navigationBar.prefersLargeTitles = true
        nav.navigationBar.barTintColor = UIColor(red: 255/255, green: 33/255, blue: 1/255, alpha: 1)
        window?.rootViewController = nav

        return true
    }
}
