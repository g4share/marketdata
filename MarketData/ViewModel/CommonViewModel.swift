//
//  CommonViewModel.swift
//  MarketData
//
//  Created by Ghenadie Munteanu on 7/13/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class CommonViewModel<T, P> {

    var startLoading: (() -> Void)?
    var dataReceived: ((P?, T) -> Void)?
    var endLoading: (() -> Void)?

    func load(persisted: P? = nil, dataLoadedHandler: @escaping ((ResponseHandler<T>) -> Void)) {
        let handler = ResponseHandler<T>(onNext: { [weak self] _, data in
            guard let safeData = data else { return }

            DispatchQueue.main.async {
                self?.dataReceived?(persisted, safeData)
            }
            }, onCompleted: { [weak self] in
                DispatchQueue.main.async {
                    self?.endLoading?()
                }
        })

        self.startLoading?()

        dataLoadedHandler(handler)
    }
}
