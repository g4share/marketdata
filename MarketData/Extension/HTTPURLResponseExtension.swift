//
//  HTTPURLResponseExtension.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/12/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

extension HTTPURLResponse {

    var isInformational: Bool {
        return (100...199).contains(self.statusCode)
    }

    var isSuccess: Bool {
        return (200...299).contains(self.statusCode)
    }

    var isRedirect: Bool {
        return (300...399).contains(self.statusCode)
    }

    var isClientError: Bool {
        return (400...499).contains(self.statusCode)
    }

    var isServerError: Bool {
        return (500...599).contains(self.statusCode)
    }

    var isError: Bool {
        return isClientError || isServerError
    }
}
