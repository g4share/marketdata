//
//  ResponseHandler.swift
//  MarketData
//
//  Created by Ghenadie Munteanu on 7/13/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class ResponseHandler<T> {
    typealias NextHandler<T> = (Int, T?) -> Void

    typealias ErrorHandler = (Int) -> Void
    typealias CompleteHandler = () -> Void

    var next: NextHandler<T>?
    var error: ErrorHandler?
    var completed: CompleteHandler?

    init(onNext: NextHandler<T>? = nil,
         onError: ErrorHandler? = nil,
         onCompleted: CompleteHandler? = nil) {
        self.next = onNext
        self.error = onError
        self.completed = onCompleted
    }
}
