//
//  Config.swift
//  MarketData
//
//  Created by Ghenadie Munteanu on 7/13/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

struct Config {
    let endPoints = Endpoints()
}

struct Endpoints {
    var base = URL(string: "https://api.iextrading.com/")!
    var logo = URL(string: "https://storage.googleapis.com/")!
}
