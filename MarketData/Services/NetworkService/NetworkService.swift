//
//  NetworkService.swift
//  MarketData
//
//  Created by Ghenadie Munteanu on 7/13/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

protocol NetworkService {
    func get<T>(url: URL, handler: ResponseHandler<T>, resultProcessor: ResponseProcessor<T>?)
}

class NetworkServiceImpl: NetworkService {

    var urlSession: URLSession

    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }

    func get<T>(url: URL, handler: ResponseHandler<T>, resultProcessor: ResponseProcessor<T>?) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        dataTask(handler: handler, resultProcessor: resultProcessor, urlRequest: request)
    }

    private func dataTask<T>(handler: ResponseHandler<T>,
                             resultProcessor: ResponseProcessor<T>?,
                             urlRequest: URLRequest) {
        let task = urlSession.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) in
            self.handleResponse(handler: handler,
                                resultProcessor: resultProcessor,
                                data: data,
                                response: response,
                                error: error)
        }

        task.resume()
    }

    private func handleResponse<T>(handler: ResponseHandler<T>,
                                   resultProcessor: ResponseProcessor<T>?,
                                   data: Data?,
                                   response: URLResponse?,
                                   error: Error?) {
        guard let validResponse = response as? HTTPURLResponse else {
            return
        }

        if validResponse.isError {
            handler.error?(validResponse.statusCode)
            handler.completed?()
            return
        }

        if validResponse.isSuccess {
            let processedData = resultProcessor?.process(data: data)
            handler.next?(validResponse.statusCode, processedData)
            handler.completed?()
        }
    }
}
