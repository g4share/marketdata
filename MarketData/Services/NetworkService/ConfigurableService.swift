//
//  ConfigurableService.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/11/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class ConfigurableService {

    private let endpoints: Endpoints

    private let service: NetworkService

    init(endpoints: Endpoints) {
        self.endpoints = endpoints
        let configuration = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: configuration)

        service = NetworkServiceImpl(urlSession: urlSession)
    }

    func get<T>(relativeUrl: String, handler: ResponseHandler<T>?, resultProcessor: ResponseProcessor<T>?) {
        if let url = URL(string: relativeUrl, relativeTo: endpoints.base), let handler = handler {
            service.get(url: url, handler: handler, resultProcessor: resultProcessor)
        }
    }
}
