//
//  ResponseProcessor.swift
//  MarketData
//
//  Created by Ghenadie Munteanu on 7/13/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class ResponseProcessor<T> {
    func process(data: Data?) -> T? {
        preconditionFailure("This method must be overridden")
    }
}

class DecodableProcessor<T: Decodable> : ResponseProcessor<T> {

    override func process(data: Data?) -> T? {
        guard let data = data else { return nil }

        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch let jsonErr {
            print(jsonErr)
            return nil
        }
    }
}

class DateFormattedDecodableProcessor<T: Decodable> : ResponseProcessor<T> {

    let dateFormatter: DateFormatter

    init(withFormatter dateFormatter: DateFormatter) {
        self.dateFormatter = dateFormatter
    }

    override func process(data: Data?) -> T? {
        guard let data = data else { return nil }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)

        do {
            return try decoder.decode(T.self, from: data)
        } catch let jsonErr {
            print(jsonErr)
            return nil
        }
    }
}
