//
//  CompanyDetailsViewController.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/12/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import UIKit

class CompanyDetailsViewController: UIViewController {

    @IBOutlet weak var ceoStaticLbl: UILabel!

    @IBOutlet weak var webSiteLbl: UILabel!
    @IBOutlet weak var ceoLbl: UILabel!
    @IBOutlet weak var sectorLbl: UILabel!
    @IBOutlet weak var descr: UILabel!

    @IBOutlet weak var stockChartView: StockChartView!

    private var viewModel: CompanyDetailsViewModel?
    var endpoints: Endpoints? {
        didSet {
            guard let safeEndpoints = endpoints else { return }
            viewModel = CompanyDetailsViewModel(endpoints: safeEndpoints)
        }
    }

    var symbol: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        populate()

        guard let symbol = symbol, let safeViewModel = viewModel else { return }

        safeViewModel.dataReceived = {[weak self] _, company in
            self?.populate(with: company)
        }
        safeViewModel.load(symbol: symbol)

        stockChartView.endpoints = endpoints
        stockChartView.loadChart(symbol: symbol, for: "1d")
    }

    private func populate(with company: Company? = nil) {
        ceoStaticLbl.isHidden = company == nil

        title = company.flatMap({$0.companyName})

        webSiteLbl.text = company.flatMap({$0.website})
        ceoLbl.text = company.flatMap({$0.CEO})
        sectorLbl.text = company.flatMap({$0.sector})
        descr.text = company.flatMap({$0.description})
    }
}
