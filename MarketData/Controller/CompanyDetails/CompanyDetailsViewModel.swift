//
//  CompanyDetailsViewModel.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/12/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class CompanyDetailsViewModel: CommonViewModel<Company, Void> {
    let service: CompanyService

    init(endpoints: Endpoints) {
        service = CompanyService(endpoints: endpoints)
    }

    func load(symbol: String) {
        let dataLoaded: (ResponseHandler<Company>?) -> Void = { [weak self] handler in
            self?.service.company(symbol: symbol, handler: handler)
        }
        load(dataLoadedHandler: dataLoaded)
    }
}
