//
//  Company.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/18/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

struct Company: Decodable {
    let symbol: String
    let companyName: String
    let exchange: String
    let website: String
    let description: String
    let CEO: String
    let sector: String
}
