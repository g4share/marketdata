//
//  StocksViewModel.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/12/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class StocksViewModel: CommonViewModel<[Symbol], Void> {
    let service: StockService

    init(endpoints: Endpoints) {
        service = StockService(endpoints: endpoints)
    }

    func load() {
        let dataLoaded: (ResponseHandler<[Symbol]>?) -> Void = { [weak self] handler in
            self?.service.stocks(handler: handler)
        }
        load(dataLoadedHandler: dataLoaded)
    }
}
