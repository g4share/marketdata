//
//  Stock.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/14/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class Symbol: Decodable {
    let symbol: String
    let name: String
    let date: Date
    let isEnabled: Bool
    let type: String

    var logoUrl: URL?

    init(symbol: String, name: String, date: Date, isEnabled: Bool, type: String) {
        self.symbol = symbol
        self.name = name
        self.date = date
        self.isEnabled = isEnabled
        self.type = type
    }

    func prepare(rootLogo: URL) {
        logoUrl = URL(string: "iex/api/logos/\(symbol).png", relativeTo: rootLogo)
    }
}
