//
//  DataService.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/12/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class StockService: ConfigurableService {

    private let symbols = "1.0/ref-data/symbols"

    func stocks(handler: ResponseHandler<[Symbol]>?) {
        get(relativeUrl: symbols,
            handler: handler,
            resultProcessor: DateFormattedDecodableProcessor(withFormatter: DateFormatter.iso8601))
    }
}
