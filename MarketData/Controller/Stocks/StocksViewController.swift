//
//  StocksViewController.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/15/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import UIKit
import SDWebImage

class StocksViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var activityStack: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private var viewModel: StocksViewModel?
    var endpoints: Endpoints? {
        didSet {
            guard let safeEndpoints = endpoints else { return }
            viewModel = StocksViewModel(endpoints: safeEndpoints)
        }
    }

    private var symbols = [Symbol]()

    private let identifier = "SymbCollectionViewCell"
    private let detailsIdentifier = "CompanyDetailsViewController"

    override func viewDidLoad() {
        super.viewDidLoad()

        let nibCell = UINib(nibName: identifier, bundle: nil)
        collectionView.register(nibCell, forCellWithReuseIdentifier: identifier)

        guard let safeViewModel = viewModel else { return }

        safeViewModel.startLoading = { [weak self] () in
            self?.startActivity()
        }
        safeViewModel.dataReceived = { [weak self] _, symbols in
            self?.dataReceived(symbols: symbols)
        }
        safeViewModel.endLoading = { [weak self] () in
            self?.stopActivity()
        }

        safeViewModel.load()
    }

    private func startActivity() {
        view.bringSubview(toFront: activityStack)
        activityIndicator.startAnimating()
    }

    private func stopActivity() {
        activityIndicator.stopAnimating()
        view.sendSubview(toBack: activityStack)
    }

    private func dataReceived(symbols: [Symbol]) {
        if let safeEndpoints = endpoints {
            symbols.forEach({ symb in symb.prepare(rootLogo: safeEndpoints.logo) })
        }

        self.symbols = symbols
        collectionView.reloadData()
    }
}

extension StocksViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfColums: CGFloat = 2
        let width = collectionView.frame.size.width
        let xInsets: CGFloat = 10
        let cellSpacing: CGFloat = 5

        let cellSize = (width / numberOfColums) - (xInsets + cellSpacing)

        return CGSize(width: cellSize, height: cellSize)
    }
}

extension StocksViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return symbols.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let queueCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)

        if let cell = queueCell as? SymbCollectionViewCell {
            cell.populate(with: symbols[indexPath.row])
        }

        return queueCell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let companyDetailsController = CompanyDetailsViewController(nibName: detailsIdentifier, bundle: nil)
        companyDetailsController.endpoints = endpoints
        companyDetailsController.symbol = symbols[indexPath.item].symbol
        self.navigationController?.pushViewController(companyDetailsController, animated: true)
    }
}
