//
//  SymbCollectionViewCell.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/11/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import UIKit

class SymbCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 10
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0.5
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        logoView.sd_cancelCurrentImageLoad()
        logoView.image = nil

        hideActivity()
    }

    func populate(with symbol: Symbol) {
        showActivity()

        logoView.sd_setImage(with: symbol.logoUrl,
                             placeholderImage: UIImage(named: "kitty"),
                             completed: { [weak self] (_, _, _, _) in
                                self?.hideActivity()
        })
        nameLabel.text = symbol.name
    }

    private func showActivity() {
        logoView.isHidden = true
        activityIndicator.isHidden = false
        bringSubview(toFront: activityIndicator)
        activityIndicator.startAnimating()
    }

    private func hideActivity() {
        activityIndicator.stopAnimating()
        sendSubview(toBack: activityIndicator)
        activityIndicator.isHidden = true
        logoView.isHidden = false
    }
}
