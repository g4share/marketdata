//
//  CompanyService.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/18/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class CompanyService: ConfigurableService {
    func company(symbol: String, handler: ResponseHandler<Company>?) {
        let company = "1.0/stock/\(symbol)/company"

        get(relativeUrl: company, handler: handler, resultProcessor: DecodableProcessor())
    }
}
