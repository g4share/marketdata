//
//  StockChartView.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/9/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import UIKit
import Charts

class StockChartView: CandleStickChartView, ChartViewDelegate {

    private let periodService = PeriodService()

    private var viewModel: StockChartViewModel?
    var endpoints: Endpoints? {
        didSet {
            guard let safeEndpoints = endpoints else { return }
            viewModel = StockChartViewModel(endpoints: safeEndpoints)
        }
    }

    private var symbol: String = ""

    private var scale: CGFloat = 1.0
    private var currentScale: Int = 1

    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
    }

    @objc func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        scale *= scaleX
        let tempScale = Int(scale)
        if tempScale != currentScale, let periodInfo = periodService.getPeriodInfo(byIndex: tempScale) {
            currentScale = tempScale
            loadChart(symbol: symbol, for: periodInfo.period)
        }
    }

    func loadChart(symbol: String, for period: String) {
        self.symbol = symbol

        guard let safeViewModel = viewModel else { return }

        safeViewModel.dataReceived = {[weak self] periodInfo, chartData in
            guard let safePeriodInfo = periodInfo else { return }
            self?.showChart(periodInfo: safePeriodInfo, chartData: chartData)
        }

        let periodInfo = periodService.getPeriodInfoOrFirst(byPeriod: period)
        safeViewModel.load(periodInfo: periodInfo, symbol: symbol, withFormatter: periodInfo.dateFormatter)
    }

    private func showChart(periodInfo: PeriodInfo, chartData: [ChartData]?) {

        guard let chartData = chartData,
                let earliestDate = chartData.min(by: { $0.dateTime() < $1.dateTime() })?.dateTime()
                else { return }

        let candleData = chartData.map({(chartData: ChartData) -> CandleChartDataEntry in
            let coordX = periodInfo.coordX(earliestDate, chartData.dateTime())
            return CandleChartDataEntry(x: coordX,
                                        shadowH: chartData.high,
                                        shadowL: chartData.low,
                                        open: chartData.open ?? -1,
                                        close: chartData.close ?? -1)
        })

        let dataSet = CandleChartDataSet(values: candleData, label: periodInfo.period)

        let data = CandleChartData(dataSet: dataSet)
        self.data = data
    }
}
