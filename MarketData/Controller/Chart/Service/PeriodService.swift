//
//  PeriodService.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/12/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class PeriodService {
    typealias DiffGetterClosure = (DateComponents) -> Int?

    private static func preparePeriodInfo(for period: String,
                                          component: Calendar.Component,
                                          formatter: DateFormatter,
                                          stepHandler: @escaping DiffGetterClosure) -> PeriodInfo {

        func diff(fromDate: Date,
                  toDate: Date,
                  component: Calendar.Component,
                  componentGetter: DiffGetterClosure) -> Double {
            let components = Calendar.current.dateComponents([component], from: fromDate, to: toDate)
            return Double(stepHandler(components)!)
        }

        return PeriodInfo(period: period, dateFormatter: formatter) { ( earliestDate, currentDate) -> (Double) in
            return diff(fromDate: earliestDate, toDate: currentDate, component: component, componentGetter: stepHandler)
        }
    }

    private let periodsInfo = [
        preparePeriodInfo(for: "1d",
                          component: .minute,
                          formatter: DateFormatter.yyyyMMdd,
                          stepHandler: { (components: DateComponents) -> Int? in
                            return components.minute
        }),
        preparePeriodInfo(for: "1m",
                          component: .day,
                          formatter: DateFormatter.iso8601,
                          stepHandler: { (components: DateComponents) -> Int? in
                            return components.day
        }),
        preparePeriodInfo(for: "3m",
                          component: .day,
                          formatter: DateFormatter.iso8601,
                          stepHandler: { (components: DateComponents) -> Int? in
                            return components.day
        }),
        preparePeriodInfo(for: "6m",
                          component: .day,
                          formatter: DateFormatter.iso8601,
                          stepHandler: { (components: DateComponents) -> Int? in
                            return components.day
        }),
        preparePeriodInfo(for: "1y",
                          component: .day,
                          formatter: DateFormatter.iso8601,
                          stepHandler: { (components: DateComponents) -> Int? in
                            return components.day
        }),
        preparePeriodInfo(for: "2y",
                          component: .day,
                          formatter: DateFormatter.iso8601,
                          stepHandler: { (components: DateComponents) -> Int? in
                            return components.day
        }),
        preparePeriodInfo(for: "5y",
                          component: .day,
                          formatter: DateFormatter.iso8601,
                          stepHandler: { (components: DateComponents) -> Int? in
                            return components.day
        })
    ]

    func getPeriodInfo(byIndex index: Int) -> PeriodInfo? {
        return index >= 0 && index < periodsInfo.count
            ? periodsInfo[index]
            : nil
    }

    func getPeriodInfoOrFirst(byPeriod period: String) -> PeriodInfo {
        return periodsInfo.first(where: { $0.period == period }) ?? periodsInfo[0]
    }
}
