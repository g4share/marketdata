//
//  ChartService.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/18/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class ChartService: ConfigurableService {

    func chart(symbol: String,
               forPeriod period: String = "",
               withFormatter formatter: DateFormatter,
               handler: ResponseHandler<[ChartData]>?) {
        let chartUrl  = "1.0/stock/\(symbol)/chart/\(period)"

        get(relativeUrl: chartUrl, handler: handler, resultProcessor: DecodableProcessor())
    }
}
