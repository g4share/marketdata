//
//  SymbolInfo.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/9/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

struct PeriodInfo {
    let period: String
    let dateFormatter: DateFormatter
    let coordX: (Date, Date) -> (Double)
}
