//
//  ChartInfo.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 6/18/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

struct ChartData: Decodable {
    let date: String
    let minute: String?
    let open: Double?
    let high: Double
    let low: Double
    let close: Double?
    let volume: Int

    let label: String

    func dateTime() -> Date {
        guard let minute = minute else {
            return DateFormatter.iso8601.date(from: date) ?? Date()
        }

        return DateFormatter.yyyyMMddWithTime.date(from: "\(date) \(minute)") ?? Date()
    }
}
