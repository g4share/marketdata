//
//  StockChartViewModel.swift
//  MarchetData
//
//  Created by Ghenadie Munteanu on 7/12/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

class StockChartViewModel: CommonViewModel<[ChartData], PeriodInfo> {
    let service: ChartService

    init(endpoints: Endpoints) {
        service = ChartService(endpoints: endpoints)
    }

    func load(periodInfo: PeriodInfo, symbol: String, withFormatter formatter: DateFormatter) {
        let dataLoaded: (ResponseHandler<[ChartData]>?) -> Void = { [weak self] handler in
            self?.service.chart(symbol: symbol,
                                forPeriod: periodInfo.period,
                                withFormatter: formatter,
                                handler: handler)
        }
        load(persisted: periodInfo, dataLoadedHandler: dataLoaded)
    }
}
