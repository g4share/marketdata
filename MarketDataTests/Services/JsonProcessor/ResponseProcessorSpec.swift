//
//  ResponseProcessorSpec.swift
//  MarketDataTests
//
//  Created by Ghenadie Munteanu on 7/13/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Quick
import Nimble
@testable import MarketData

class ResponseProcessorSpec: QuickSpec {
    override func spec() {

        describe("DecodableProcessor") {
            let processor = DecodableProcessor<DecodableData>()

            context("when try to deserialize 'no data'") {
                it("should return nil") {
                    expect(processor.process(data: nil)).to(beNil())
                }
            }

            context("when try to deserialize invalid json") {
                let data = "{\"ke}".data(using: .utf8)
                it("should return nil") {
                    expect(processor.process(data: data)).to(beNil())
                }
            }

            context("when try to deserialize valid json") {
                let key = "The Answer to the Ultimate Question of Life, The Universe, and Everything."
                let value = 42
                let data = "{\"key\":\"\(key)\",\"value\":\(value)}".data(using: .utf8)
                it("should return the expected object") {
                    let decodedObject = processor.process(data: data)
                    expect(decodedObject).notTo(beNil())

                    expect(decodedObject!.key).to(equal(key))
                    expect(decodedObject!.value).to(equal(value))
                    expect(decodedObject!.date).to(beNil())
                }
            }
        }

        describe("DateFormattedDecodableProcessor") {
            let processor = DateFormattedDecodableProcessor<DecodableData>(withFormatter: DateFormatter.yyyyMMdd)

            context("when try to deserialize 'no data'") {
                it("should return nil") {
                    expect(processor.process(data: nil)).to(beNil())
                }
            }

            context("when try to deserialize invalid json") {
                let data = "{\"ke}".data(using: .utf8)
                it("should return nil") {
                    expect(processor.process(data: data)).to(beNil())
                }
            }

            context("when try to deserialize wrong Date format json") {
                let data = "{\"key\":\"\",\"value\":0,\"date\":\"2018-08-08\"}".data(using: .utf8)
                it("should return nil") {
                    expect(processor.process(data: data)).to(beNil())
                }
            }

            context("when try to deserialize right Date format json") {
                let data = "{\"key\":\"\",\"value\":0,\"date\":\"20180808\"}".data(using: .utf8)
                it("should return the expected object") {
                    let decodedObject = processor.process(data: data)
                    expect(decodedObject).notTo(beNil())

                    let year = Calendar.current.component(.year, from: decodedObject!.date!)
                    expect(year).to(equal(2018))
                }
            }
        }
    }
}

struct DecodableData: Decodable {
    let key: String
    let value: Int
    let date: Date?
}
