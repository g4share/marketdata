//
//  NetworkServiceSpec.swift
//  MarketDataTests
//
//  Created by Ghenadie Munteanu on 7/14/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

import Quick
import Nimble
@testable import MarketData

class NetworkServiceSpec: QuickSpec {
    override func spec() {
        describe("NetworkService") {
            let endpoints = Endpoints(base: URL(string: "https://postman-echo.com/")!,
                                      logo: URL(string: "https://google.com/")!)

            let networkService = ConfigurableService(endpoints: endpoints)
            let networkServiceHelper = NetworkServiceHelper(service: networkService, timeout: TimeInterval(5))

            context("when try to make request") {
                let uuid = NSUUID().uuidString.lowercased()

                it("should returned expected data") {

                    let result: TestData? = networkServiceHelper.expectResult(relativeUrl: "get?key=\(uuid)",
                                                                              resultProcessor: DecodableProcessor())

                    expect(result).notTo(beNil())
                    expect(result?.url).to(equal("\(endpoints.base)get?key=\(uuid)"))
                    expect(result?.args.key).to(equal(uuid))
                }
            }

            context("when try to make not valid request") {
                it("should cal error callback") {

                    let result: TestData? = networkServiceHelper.expectError(relativeUrl: "nonExistingUrl",
                        resultProcessor: DecodableProcessor())

                    expect(result).to(beNil())
                }
            }
        }
    }

    struct TestData: Decodable {
        let url: String
        let args: Args
    }

    struct Args: Decodable {
        let key: String
    }
}
