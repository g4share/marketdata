//
//  NetworkServiceHelper.swift
//  MarketDataTests
//
//  Created by Ghenadie Munteanu on 7/16/18.
//  Copyright © 2018 Ghenadie Munteanu. All rights reserved.
//

import Foundation

import Quick
import Nimble
@testable import MarketData

class NetworkServiceHelper {

    let service: ConfigurableService
    let timeout: TimeInterval

    init(service: ConfigurableService, timeout: TimeInterval = AsyncDefaults.Timeout) {
        self.service = service
        self.timeout = timeout
    }

    func expectResult<T: Decodable>(relativeUrl: String,
                                    resultProcessor: ResponseProcessor<T>?) -> T? {
        return callService(relativeUrl: relativeUrl,
                           resultProcessor: resultProcessor,
                           expected: (true, false))
    }

    func expectError<T: Decodable>(relativeUrl: String,
                                   resultProcessor: ResponseProcessor<T>?) -> T? {
        return callService(relativeUrl: relativeUrl,
                           resultProcessor: resultProcessor,
                           expected: (false, true))
    }

    private func callService<T: Decodable>(relativeUrl: String,
                                           resultProcessor: ResponseProcessor<T>?,
                                           expected: (Bool, Bool)) -> T? {
        var callbackCalled = (wasNext: false, wasError: false)

        var result: T?

        waitUntil(timeout: timeout) {[weak self] done in

            let handler = ResponseHandler<T>(onNext: { (_, data) in
                result = data
                callbackCalled.wasNext = true
            }, onError: { (_) in
                callbackCalled.wasError = true
            }, onCompleted: {
                expect(callbackCalled.wasNext).to(equal(expected.0))
                expect(callbackCalled.wasError).to(equal(expected.1))

                done()
            })

            self?.service.get(relativeUrl: relativeUrl,
                              handler: handler,
                              resultProcessor: resultProcessor)
        }

        return result
    }
}
